package com.sourceit.broadcastreceiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;


public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MyReceiver", "onReceive");

        if (isAirplaneModeOn(context)) {
            onToastNotification(context, "mode on" );
            Log.d("MyReceiver", "mode on");
        } else {
            Log.d("MyReceiver", "mode off");
        }
    }

    private static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;

    }

    public void onToastNotification(Context context, String title) {
        Intent notificationIntent = new Intent(context, MainActivity.class);


        PendingIntent contentIntent = PendingIntent.getActivity(context,
                101, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);


        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Title")
                .setContentText("Text, my text");

        Notification n = builder.build();
        nm.notify(102, n);
    }
}

